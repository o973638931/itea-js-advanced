//
// var button = buttonContainer.querySelectorAll('#buttonContainer button');
// button[0].onclick = function() {
//     console.log(tabContainer.querySelector('[data-tab="1"]'));
//     // tabContainer.querySelector('[data-tab="1"]').classList.add('active');
//     tabContainer.querySelector('[data-tab="2"]').classList.remove('active');
//     tabContainer.querySelector('[data-tab="3"]').classList.remove('active');
// };
// button[1].onclick = function() {
//     console.log(tabContainer.querySelector('[data-tab="2"]'));
//     // tabContainer.querySelector('[data-tab="2"]').classList.add('active');
//     tabContainer.querySelector('[data-tab="1"]').classList.remove('active');
//     tabContainer.querySelector('[data-tab="3"]').classList.remove('active');
// };
// button[2].onclick = function() {
//     console.log(tabContainer.querySelector('[data-tab="3"]'));
//     // tabContainer.querySelector('[data-tab="3"]').classList.add('active');
//     tabContainer.querySelector('[data-tab="1"]').classList.remove('active');
//     tabContainer.querySelector('[data-tab="2"]').classList.remove('active');
// };

var buttonS = document.querySelectorAll(".showButton");
var tabS = document.getElementsByClassName('tab');
tabS = Array.from(tabS);
console.log(tabS);

function changeTab(e) {
    var tabActiveNumber = e.target.dataset.tab;
    console.log('tabActiveNumber: ', tabActiveNumber);

    for (var i = 0; i < tabS.length; i++) {
        tabS[i].classList.remove('active');
        var activeTab = tabS[i].dataset.tab;
        if (activeTab === tabActiveNumber) {
            tabS[i].classList.add('active');
        }
    }
}

buttonS.forEach(function (item) {
    item.addEventListener("click", changeTab);
});

/*

    Задание 1.

    Написать скрипт который будет будет переключать вкладки по нажатию
    на кнопки в хедере.

    Главное условие - изменять файл HTML нельзя.

    Алгоритм:
      1. Выбрать каждую кнопку в шапке
         + бонус выбрать одним селектором

      2. Повесить кнопку онклик
          button1.onclick = function(event) {

          }
          + бонус: один обработчик на все три кнопки

      3. Написать функцию которая выбирает соответствующую вкладку
         и добавляет к ней класс active

      4. Написать функцию hideAllTabs которая прячет все вкладки.
         Удаляя класс active со всех вкладок

    Методы для работы:

      getElementById
      querySelector
      classList
      classList.add
      forEach
      onclick

      element.onclick = function(event) {
        // do stuff ...
      }

  */
